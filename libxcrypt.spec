%define  libdir /lib64
Name:           libxcrypt
Version:        4.4.26
Release:        1
Summary:        Extended crypt library for DES, MD5, Blowfish and others
License:        LGPLv2+ and BSD and Public Domain
URL:            https://github.com/besser82/%{name}
Source0:        https://github.com/besser82/%{name}/archive/v%{version}.tar.gz

Patch9000:      add-sm3-crypt-support.patch

BuildRequires:  perl >= 5.14.0
BuildRequires:  autoconf libtool fipscheck
Obsoletes:      %{name}-common < %{version}-%{release}
Provides:       %{name}-common%{?_isa} = %{version}-%{release} %{name}%{?_isa} = %{version}-%{release}
Provides:       %{name}-common = %{version}-%{release}
Provides:       %{name}-sm3 = %{version}-%{release}

%description
libxcrypt is a modern library for one-way hashing of passwords.
It supports a wide variety of both modern and historical hashing
methods: yescrypt, gost-yescrypt, scrypt, bcrypt, sha512crypt,
sha256crypt, md5crypt, SunMD5, sha1crypt, NT, bsdicrypt, bigcrypt,
and descrypt. It provides the traditional Unix crypt and crypt_r
interfaces, as well as a set of extended interfaces pioneered by
Openwall Linux, crypt_rn, crypt_ra, crypt_gensalt, crypt_gensalt_rn,
and crypt_gensalt_ra.
libxcrypt is intended to be used by login(1), passwd(1), and other
similar programs; that is, to hash a small number of passwords during
an interactive authentication dialogue with a human. It is not suitable
for use in bulk password-cracking applications, or in any other situation
where speed is more important than careful handling of sensitive data.
However, it is intended to be fast and lightweight enough for use in
servers that must field thousands of login attempts per minute.

%package  devel
Summary:    Development files for %{name}
Requires:   %{name} = %{version}-%{release}
Requires:   glibc-devel glibc-static
Obsoletes:  %{name}-static < %{version}-%{release}
Provides:   %{name}-static %{name}-static%{?_isa} %{name}-devel%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package  help
Summary:  Man page for API of %{name}
BuildArch: noarch

%description  help
%{summary}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -fiv

%configure \
    --libdir=%{libdir} \
    --disable-silent-rules \
    --enable-shared \
    --enable-static \
    --with-pkgconfigdir=%{_libdir}/pkgconfig \
    --enable-obsolete-api=glibc

%make_build


%install
%make_install

%{_bindir}/find %{buildroot} -name '*.la' -print -delete

%check
make check

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%license COPYING.LIB LICENSING
%doc NEWS README README.md THANKS AUTHORS
%{libdir}/libcrypt.so.*

%files devel
%doc ChangeLog TODO TODO.md
%{libdir}/{libcrypt.so,libxcrypt.so}
%{_includedir}/{crypt.h,xcrypt.h}
%{_libdir}/pkgconfig/{libcrypt,%{name}}.pc
%{libdir}/{libcrypt.a,libxcrypt.a}

%files help
%{_mandir}/man3/crypt{,_r,_ra,_rn}.3.*
%{_mandir}/man3/crypt_gensalt{,_ra,_rn}.3.*
%{_mandir}/man3/crypt_checksalt.3.*
%{_mandir}/man3/crypt_preferred_method.3.*
%{_mandir}/man5/crypt.5.*


%changelog
* Thu Dec 30 2021 yixiangzhike <yixiangzhike007@163.com> - 4.4.26-1
- update to 4.4.26

* Tue Dec 28 2021 houmingyong<houmingyong@huawei.com> - 4.4.17-3
- add sm3 DT test case

* Mon Dec 27 2021 wangyu <wangyu283@huawei.com> - 4.4.17-2
- add sm3 crypt support

* Thu Jan 21 2021 wangchen <wangchen137@huawei.com> - 4.4.17-1
- update to 4.4.17

* Tue Sep 8 2020 wangchen <wangchen137@huawei.com> - 4.4.16-2
- modify the URL of Source0

* Tue Jul 28 2020 yang_zhuang_zhuang <yangzhuangzhuang1@huawei.com> - 4.4.16-1
- update version to 4.4.16

* Mon Mar 9 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.4.8-4
- add link files README.md and TODO.md

* Wed Jan 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.4.8-3
- fix build problem

* Mon Oct 28 2019 shenyangyang <shenyangyang4@huawei.com> - 4.4.8-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add provides of libxcrypt-common

* Thu Sep 4 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.4.8-1
- Package init
